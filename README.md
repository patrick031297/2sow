### Instruções para executar o projeto

```bash
## Clone este repositório
$ git clone https://patrick031297@bitbucket.org/patrick031297/2sow.git

## Instale o json-server
$ yarn global add json-server

## Rode o servidor
$ json-server --watch db.json --delay 2000 --port 5000

## Instale as dependências na raiz do projeto (pasta 2sow)
$ yarn install

## Execute a aplicação
$ yarn start

```
